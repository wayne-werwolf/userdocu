Installation of openCFS and Related Software
============================================

In order to do simulations with openCFS you will require some additional software.
A typical simulation workflow requires the following steps and corresponding software tools: 

1. [pre-processing](PrePorcessors.md) (geometry definition & mesh generation): openCFS supports various common mesh formats.
2. [input generation](XML-Editors.md): openCFS input is in XML-format, i.e. the input can be written by any text editor.
3. computation: [install openCFS](CFS.md)
4. [post-processing](ParaView.md): we visualize field results using ParaView and a **custom plugin** to read openCFS' HDF5 data format.
